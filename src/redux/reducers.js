import { combineReducers } from "redux";
import { ADD_COUNTER, MAX_PRICE, MIN_PRICE, REMOVE_COUNTER, SORT_MANUFACTURER } from "./types";


const defaultState = {
  goods: [],
  sortManufacturer: false,
  sortMinPrice: false,
  sortMaxPrice: false
}

const cartReducer = (state = defaultState, action)=>{
  switch(action.type){
    case ADD_COUNTER:
      return{...state, goods: state.goods.concat([action.itemId])}
    case REMOVE_COUNTER:
      return {...state, goods: state.goods.filter(item => item !== action.itemId)}
    case SORT_MANUFACTURER:
      return {...state, sortManufacturer: !state.sortManufacturer, sortMinPrice: false, sortMaxPrice: false}
    case MIN_PRICE:
      return {...state, sortMinPrice: !state.sortMinPrice, sortManufacturer: false, sortMaxPrice: false}
    case MAX_PRICE:
      return {...state, sortMaxPrice: !state.sortMaxPrice, sortManufacturer: false, sortMinPrice: false}
    default: return state
  }
}


export const reducer = combineReducers({
  cart: cartReducer
})
