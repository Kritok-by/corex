import { ADD_COUNTER, MAX_PRICE, MIN_PRICE, REMOVE_COUNTER, SORT_MANUFACTURER } from "./types";

export function addId(id){
  return{
    type: ADD_COUNTER,
    itemId: id
  }
}
export function removeId(id){
  return{
    type: REMOVE_COUNTER,
    itemId: id
  }
}
export function sort(){
  return{
    type: SORT_MANUFACTURER
  }
}
export function minPrice(){
  return{
    type: MIN_PRICE
  }
}
export function maxPrice(){
  return{
    type: MAX_PRICE
  }
}
