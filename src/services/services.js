import React from "react";
import Async from "react-async";
import List from "../components/Main/Lists/List/List";

const loadJson = () =>
  fetch("https://kritok-by.github.io/server/db.json")
    .then((res) => (res.ok ? res : Promise.reject(res)))
    .then((res) => res.json());



const Services = () => (
  <Async promiseFn={loadJson}>
    <Async.Loading>
      <div className="spinner-border text-light" role="status">
        <span className="sr-only">Loading...</span>
      </div>
    </Async.Loading>

    <Async.Resolved>
      {(data) => (
        <ul>
          {data.store.map((el) => (
            <List data={el} key={el.id} />
          ))}
        </ul>
      )}
    </Async.Resolved>

    <Async.Rejected>
      {(error) => `Something went wrong: ${error.message}`}
    </Async.Rejected>
  </Async>
);

const ManuSort = () => (
  <Async promiseFn={loadJson}>
    <Async.Loading>
      <div className="spinner-border text-light" role="status">
        <span className="sr-only">Loading...</span>
      </div>
    </Async.Loading>

    <Async.Resolved>
      {(data) => (
        <ul>
          {
          data.store.sort((a, b) => a.manufacturer > b.manufacturer ? 1 : -1).map((el) => (
            <List data={el} key={el.id} />
          ))}
        </ul>
      )}
    </Async.Resolved>

    <Async.Rejected>
      {(error) => `Something went wrong: ${error.message}`}
    </Async.Rejected>
  </Async>
);

const MinSort = () => (
  <Async promiseFn={loadJson}>
    <Async.Loading>
      <div className="spinner-border text-light" role="status">
        <span className="sr-only">Loading...</span>
      </div>
    </Async.Loading>

    <Async.Resolved>
      {(data) => (
        <ul>
        {
        data.store.sort((a, b) => a.maxPrice - b.maxPrice).map((el) => (
          <List data={el} key={el.id} />
        ))}
      </ul>
      )}
    </Async.Resolved>

    <Async.Rejected>
      {(error) => `Something went wrong: ${error.message}`}
    </Async.Rejected>
  </Async>
);
const MaxSort = () => (
  <Async promiseFn={loadJson}>
    <Async.Loading>
      <div className="spinner-border text-light" role="status">
        <span className="sr-only">Loading...</span>
      </div>
    </Async.Loading>

    <Async.Resolved>
    {(data) => (
        <ul>
        {
        data.store.sort((a, b) => b.maxPrice - a.maxPrice).map((el) => (
          <List data={el} key={el.id} />
        ))}
      </ul>
      )}
    </Async.Resolved>

    <Async.Rejected>
      {(error) => `Something went wrong: ${error.message}`}
    </Async.Rejected>
  </Async>
);





export {Services, ManuSort, MinSort, MaxSort};
