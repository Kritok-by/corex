import React from 'react';
import {connect} from 'react-redux';
import {sort, minPrice, maxPrice} from '../../../redux/actions'
import './Filter.scss';

class Filter extends React.Component{
  render(){
    return(
      <nav>
        <h2>SORT BY</h2>
        <ul>
          <li className='manufacturer' onClick={()=>this.props.sort()}><i/>By Manufacturer</li>
          <li className='min-price' onClick={()=>this.props.minPrice()}><i/>Minimum price</li>
          <li className='max-price' onClick={()=>this.props.maxPrice()}><i/>Maximum price</li>
        </ul>
      </nav>
    )
  }
}

const mapDispatchToProps = {
  sort,
  minPrice,
  maxPrice
}

export default connect(null, mapDispatchToProps)(Filter)
