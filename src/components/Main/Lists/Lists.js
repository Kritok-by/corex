import React from 'react';
import HeaderList from './HeaderList/HeaderList';
import {Services, ManuSort, MinSort, MaxSort} from '../../../services/services'
import './Lists.scss';
import { connect } from 'react-redux';

class Lists extends React.Component{
  render(){
    let {sortManufacturer, sortMinPrice, sortMaxPrice} = this.props.cart;
    let list = ()=>{
    switch(true){
      case sortManufacturer:
        return <ManuSort/>
      case sortMinPrice:
        return <MinSort/>
      case sortMaxPrice:
        return <MaxSort/>
      default:
        return <Services/>}}
    console.log(sortManufacturer)
    console.log(sortMinPrice)
    console.log(sortMaxPrice)
    return(
      <article>
        <h2>ON SALE</h2>
        <ul>
        <HeaderList/>
        {list()}
        </ul>
      </article>
    )
  }
}


const mapStateToProps = ({cart}) =>{
  return{
    cart
  }
}

export default connect(mapStateToProps, null)(Lists);
